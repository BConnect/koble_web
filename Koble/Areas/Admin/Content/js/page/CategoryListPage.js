﻿$(function () {
    var data = [
         { "id": "ajson1", "parent": "#", "text": "Simple root node" },
         { "id": "ajson2", "parent": "#", "text": "Root node 2" },
         { "id": "ajson3", "parent": "ajson2", "text": "Child 1" },
         { "id": "ajson4", "parent": "ajson2", "text": "Child 2" },
    ];
    $("#CategoryTree").jstree({
        "core": {
            // so that create works
            "check_callback": true,

            "data": data,
            "themes": {
                "icons": false
            }
        },
        "plugins": ["contextmenu", "dnd", "types"],
        "contextmenu": {
            "items": {
                "create": {
                    "label": "Add",
                    "action": function (obj) {
                        $('#CategoryList').jstree().create_node('#', { "id": "ajson5", "text": "newly added" }, "last", function () {
                            alert("done");
                        });
                    },
                }
            }
        }
    }).on('select_node.jstree', function (e, data) {
        $('#NewChild').removeClass('hidden');
        $('#DeleteNode').removeClass('hidden');
        $('#NewParent').removeClass('hidden');
        $('#Category_Form').find('#CategoryName').val(data.node.data.CategoryName);
        $('#Category_Form').find('#Description').val(data.node.data.Description);
        $('#UnitType option:eq(' + data.node.data.UnitType + ')').prop('selected', true);
        console.log(data.node.data);
        if (data.node.data.ImageType == "DefaultImage") {
            $('#customize_fileinput').find('[data-dismiss="fileinput"]').trigger('click.bs.fileinput', $.proxy(this.clear, this));
        }
        else {
            $('#customize_fileinput').addClass('fileinput-exists').removeClass('fileinput-new')
            $('#ImageUrl').html('<img src="' + data.node.data.ImageSrc + '" class="m-t-xs img-responsive" style="max-height:200px;max-width:200px;" alt="No Image Found">');
            $('#ImageUrl').show();
        }


    });
    $("#save").on("click", function () {
        var CategoryName = $('#Category_Form').find('#CategoryName').val().trim();
        if (CategoryName.length == 0) {
            alert("Please insert Category Name");
        } else {
            var SelectedNode = $("#CategoryTree").jstree("get_selected");
            var Description = $('#Category_Form').find('#Description').val().trim();
            var UnitType = $('#UnitType :selected').val();
            var ImageSrc = $('#ImageUrl').find('img').attr('src');//"~/Content/img/CategoryImages/Cake1.jpg"; //
            var ImageType = "UplodedImage";
            if (ImageSrc == undefined || ImageSrc == null) {
                ImageType = "DefaultImage";
            } else {
                ImageType = "UplodedImage";
            }
            console.log(ImageType);
            if (SelectedNode.length == 0) {
                $('#CategoryTree').jstree().create_node('#', { "id": generateUUID(), "text": CategoryName, data: { "CategoryName": CategoryName, "Description": Description, "UnitType": UnitType, "ImageSrc": ImageSrc, "ImageType": ImageType } }, "last", function () {
                    alert("Saved");
                });
            }
            else {
                $('#CategoryTree').jstree().create_node('' + SelectedNode + '', { "id": generateUUID(), "text": CategoryName, data: { "CategoryName": CategoryName, "Description": Description, "UnitType": UnitType, "ImageSrc": ImageSrc, "ImageType": ImageType } }, "last", function () {
                    alert("Saved");
                });
            }
            $("#CategoryTree").jstree("deselect_all");
            ResetAll();
        }
    });
});

//Reset All Data
var ResetAll = function () {
    $('#NewChild').addClass('hidden');
    $('#DeleteNode').addClass('hidden');
    $('#NewParent').addClass('hidden');
    $("#CategoryTree").jstree("deselect_all");
    $('#Category_Form').find('#CategoryName').val('');
    $('#Category_Form').find('#Description').val('');
    $('#UnitType option:eq(0)').prop('selected', true);
    $('#customize_fileinput').find('[data-dismiss="fileinput"]').trigger('click.bs.fileinput', $.proxy(this.clear, this))
}

var AddNewParent = function () {
    ResetAll();
    $("#CategoryTree").jstree("deselect_all");
}

var AddNewChild = function () {
    $('#Category_Form').find('#CategoryName').val('');
    $('#Category_Form').find('#Description').val('');
    $('#UnitType option:eq(0)').prop('selected', true);
    $('#customize_fileinput').find('[data-dismiss="fileinput"]').trigger('click.bs.fileinput', $.proxy(this.clear, this))
    var SelectedNode = $("#CategoryTree").jstree("get_selected");
    if (SelectedNode.length == 0) {
        alert("Please select the node Where You want to add Subcategory");
    }
}

var DeleteNode = function () {
    var SelectedNode = $("#CategoryTree").jstree("get_selected");
    if (SelectedNode.length == 0) {
        alert("Please select the node which You want remove form list");
    } else {
        var chk = confirm('Are you sure you want to delete this? It will also remove sub-category too');
        if (chk == true) {
            $("#CategoryTree").jstree().delete_node($("#" + SelectedNode + ""));
        } else {
            $("#CategoryTree").jstree("deselect_all");
        }
    }
    ResetAll();
}
