﻿using System;
using System.Web;
using System.Web.Optimization;

namespace Koble
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {

            // Stylesheets
            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/css/plugins.css",
                        "~/Content/css/main.css",
                        "~/Content/css/themes.css"
                        ));

            // Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8)

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/js/plugins.js",
                        "~/Content/js/app.js"
                       ));

        }
    }
}