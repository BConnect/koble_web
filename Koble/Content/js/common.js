﻿$.fn.ajaxload = function () {
    return this.each(function () {
        $(this).addClass('disabled');
        $(this).find('i').hide();
        $(this).find('i.fa-spinner').remove();
        $(this).prepend('<i class="fa fa-spinner fa-spin"></i> ');
    });
};

$.fn.ajaxloadremove = function () {
    return this.each(function () {
        $(this).removeClass('disabled');
        $(this).find('i.fa-spinner').remove();
        $(this).find('i').show();

    });
};

var generateUUID = function () {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}
